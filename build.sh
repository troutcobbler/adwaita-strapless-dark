#!/bin/bash
# Adwaita Colorizer, by Dominic Hayes, Modified by PizzaLovingNerd
# further modified by Will Elliott aka cog aka troutcobbler

if [ "$#" -lt 3 ]
then
    echo 'Please run this script with the theme name, gtk version, hex color, and target directory prefix'
    echo 'ex: "./build.sh Adwaita-strapless 3 3584e4 ~/.local/share/themes"'
    exit 1
else
    gtkthemename=$1
    gtkver=$2
    color=$3
    prefix=$4
fi

if [ "$gtkver" != "3" ] && [ "$gtkver" != "4" ]; then
    gtkver=3
fi

path=$(pwd)

if [ "$gtkver" == "3" ]; then
gtkpathname="Adwaita"
else
gtkpathname="Default"
fi

#you can use this method for a file from the repo or the later one to patch on the fly
#cp nautilus.css gtk/gtk/theme/$gtkpathname

cd gtk/gtk/theme/$gtkpathname
cp $path/_colors.scss .

# xdg-open _colors.scss >/dev/null 2>&1 &
# read -p 'The colors file should now open. Make any colour changes you want and press ENTER to continue.'
sed -i "s/3584e4/$color/g" _colors.scss
sed -i "s/2265b5/$color/g" _colors.scss

# gedit assets.svg >/dev/null 2>&1 &
# read -p 'The assets.svg file should now open. Make sure, for consistency, that the colour is changed here too. Press ENTER to continue.'
sed -i "s/3584e4/$color/g" assets.svg
sed -i "s/2265b5/$color/g" assets.svg

# temporary gtk2 fix
sed -i "s/658280/eeeeec/g" $path/gtk-2.0/gtkrc
sed -i "s/110005/2d2d2d/g" $path/gtk-2.0/gtkrc

if [ -f /usr/bin/inkscape ]; then
    sed -i 's%flatpak run org.inkscape.Inkscape%/usr/bin/inkscape%g' render-assets.sh
fi

mv assets assets-old
mkdir assets
bash ./render-assets.sh

if [ ! $? -eq 0 ]; then
    echo "Rendering assets failed. This likely means you don't have optipng or inkscape installed. Deleting gtk folder and aborting."
    cd ../../../
    git clean -fdx
    git reset --hard
    cd ../
    git clean -fdx
    git reset --hard
    exit 1
fi

for file in assets-old/*; do
    if [ ! -f "$(echo $file | sed 's%assets-old%assets%g')" ]; then
        cp -f "$file" "$(echo $file | sed 's%assets-old%assets%g')"
    fi
done
rm -rf assets-old

echo "Creating your recoloured Adwaita..."

if [ "$gtkver" = 3 ]; then
    sassc $SASSC_OPT gtk-contained.scss gtk-contained.css
else
    sassc $SASSC_OPT $gtkpathname-light.scss gtk.css
fi
if [ ! $? -eq 0 ]; then
    echo "Rendering the theme failed. This likely means you don't have sassc installed. Deleting gtk folder and aborting."
    cd ../../../
    git clean -fdx
    git reset --hard
    cd ../
    git clean -fdx
    git reset --hard
    exit 1
fi

if [ "$gtkver" = 3 ]; then
    sassc $SASSC_OPT gtk-contained-dark.scss gtk-contained-dark.css
else
    sassc $SASSC_OPT $gtkpathname-dark.scss gtk-dark.css
fi
if [ ! $? -eq 0 ]; then
    echo "Rendering the theme failed. This likely means you don't have sassc installed. Deleting gtk folder and aborting."
    cd ../../../
    git clean -fdx
    git reset --hard
    cd ../
    git clean -fdx
    git reset --hard
    exit 1
fi

if [ "$gtkver" = 3 ]; then
    echo "Making final changes to css files..."
    echo '@import url("gtk-contained.css");' > gtk.css #if this isn't done, the system just loads fallback Adwaita when you apply your theme
    echo '@import url("gtk-contained-dark.css");' > gtk-dark.css
fi

clear

echo "Theme is now being constructed..."

#mkdir -p $prefix/$gtkthemename/gtk-$gtkver.0
mkdir -p $prefix/$gtkthemename-dark/gtk-$gtkver.0

#echo "Downloading and applying app patches..."
#wget https://gitlab.gnome.org/GNOME/nautilus/-/raw/gnome-40/src/resources/css/Adwaita.css -O nautilus.css
#cat nautilus.css >> gtk-contained.css
#cat nautilus.css >> gtk-contained-dark.css

#rm nautilus.css

echo "Generating theme..."
if [ "$gtkver" = 3 ]; then
    #cp -Rf assets $prefix/$gtkthemename/gtk-$gtkver.0/assets
    #cp -f gtk.css $prefix/$gtkthemename/gtk-$gtkver.0/gtk.css
    #cp -f gtk-contained.css $prefix/$gtkthemename/gtk-$gtkver.0/gtk-contained.css
    #cp -f gtk-dark.css $prefix/$gtkthemename/gtk-$gtkver.0/gtk-dark.css
    #cp -f gtk-contained-dark.css $prefix/$gtkthemename/gtk-$gtkver.0/gtk-contained-dark.css

    cp -Rf assets $prefix/$gtkthemename-dark/gtk-$gtkver.0/assets
    cp -f gtk.css $prefix/$gtkthemename-dark/gtk-$gtkver.0/gtk.css
    cp -f gtk-contained-dark.css $prefix/$gtkthemename-dark/gtk-$gtkver.0/gtk-contained.css
    cp -f gtk-dark.css $prefix/$gtkthemename-dark/gtk-$gtkver.0/gtk-dark.css
    cp -f gtk-contained-dark.css $prefix/$gtkthemename-dark/gtk-$gtkver.0/gtk-contained-dark.css
    cp -Rf $path/gtk-2.0 $prefix/$gtkthemename-dark/
else
    cp -Rf assets $prefix/$gtkthemename/gtk-$gtkver.0/assets
    cp -f $gtkpathname-light.scss $prefix/$gtkthemename/gtk-$gtkver.0/gtk.css
    cp -f $gtkpathname-dark.scss $prefix/$gtkthemename/gtk-$gtkver.0/gtk-dark.css

    cp -Rf assets $prefix/$gtkthemename-dark/gtk-$gtkver.0/assets
    cp -f $gtkpathname-dark.scss $prefix/$gtkthemename-dark/gtk-$gtkver.0/gtk.css
    cp -f $gtkpathname-dark.scss $prefix/$gtkthemename-dark/gtk-$gtkver.0/gtk-dark.css
fi

echo "Removing downloaded files..."
cd $path/gtk
git clean -fdx
git reset --hard
cd ../
git clean -fdx
git reset --hard

echo "All done, check your new theme out!"
exit 0
